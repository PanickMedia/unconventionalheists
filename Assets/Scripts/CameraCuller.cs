﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class CameraCuller : MonoBehaviour
{

    public GameObject player;
    public GameObject o;
    public bool mustFadeBack = false;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, player.transform.position - transform.position, out hit, 30))
        {
            if ((hit.collider.gameObject.layer == LayerMask.NameToLayer("Floor" + (GameManager.instance.player.floor + 1) + "Walls" ) ) )
            {
                mustFadeBack = true;

                if (hit.collider.gameObject != o && o != null)
                {
                    FadeUp(o);
                }

                o = hit.collider.gameObject;
                FadeDown(o);
            }
            else
            {
                if (mustFadeBack)
                {
                    mustFadeBack = false;
                    FadeUp(o);
                }
            }
        }
    }

    void FadeUp(GameObject f)
    {
        if(f.GetComponent<Renderer>() != null)
            f.GetComponent<Renderer>().enabled = true;
    }

    void FadeDown(GameObject f)
    {
        if (f.GetComponent<Renderer>() != null)
            f.GetComponent<Renderer>().enabled = false;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, player.transform.position - transform.position);
    }
}