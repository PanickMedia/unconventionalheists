﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorSwitcher : MonoBehaviour {
    public Renderer[] render;
    public bool common = false;
    private void Start()
    {
        render = GetComponentsInChildren<Renderer>();
    }

    public void SetVisible()
    {
        foreach(Renderer item in render)
        {
            item.enabled = true;
        }
    }

    public void SetHidden()
    {
        foreach (Renderer item in render)
        {
            item.enabled = false;
        }
    }
}
