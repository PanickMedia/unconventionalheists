﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using BehaviorDesigner.Runtime;

public class WakingTest : MonoBehaviour {
    public Behavior trees;
    public CharacterController controller;
    public Animator animator;

    public GameObject head;
    public GameObject playerHead;

    // Use this for initialization
	void Start () {
        trees = GetComponent<BehaviorTree>();
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }
	public void SendWakingSound(GameObject location)
    {
        trees.SendEvent("noise");
        trees.SetVariableValue("target", (object)location);
        Debug.Log("what the shit");
    }
	// Update is called once per frame
	void Update () {
        animator.SetInteger("speed", (int)controller.velocity.magnitude);
        lookAbout();
	}

    public void lookAbout()
    {
        RaycastHit hit;
        bool blocked = false;
        if (Physics.Raycast(head.transform.position, playerHead.transform.position - head.transform.position, out hit, 20f ) ) 
        {
            if (hit.transform.gameObject != playerHead)
            {
                blocked = true;
                GameManager.instance.SeenIndicator.SetActive(false);
                Debug.DrawLine(head.transform.position, hit.point);
            }
            else if (blocked == false && hit.transform.gameObject == playerHead)
            {
                var direction = (GameManager.instance.player.transform.position - transform.position).normalized;
                float dot = Vector3.Dot(direction, transform.forward);
                float dot2 = Vector2.Dot(new Vector2(direction.x, direction.z), new Vector2(transform.forward.x, transform.forward.z));
                if ( dot2 >= 0.55f)
                {
                    Debug.DrawLine(head.transform.position, hit.point);
                    GameManager.instance.SeenIndicator.SetActive(true);
                    //Debug.Log(dot2);
                }
                else
                {
                    GameManager.instance.SeenIndicator.SetActive(false);
                }
            }
            if( (transform.position - GameManager.instance.player.transform.position).magnitude < 3f)
            {
                GameManager.instance.SeenIndicator.SetActive(true);
            }
        }
    }

}
