﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    public CharacterController controller;
    public Animator animator;
    public float speed = 10;
    public float speedMultiplier = 1;
    public int floor;
    public float sensitivity = 25;
    public bool movelocked;
    public stance currentStance;
    public enum stance
    {
        crouched,
        standing
    }

    // Use this for initialization
    void Start()
    {
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        currentStance = stance.standing;
    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0) && controller.isGrounded && !movelocked)
        {
            controller.SimpleMove(transform.forward * Input.GetAxis("Vertical") * speed * speedMultiplier + transform.right * Input.GetAxis("Horizontal") * speed);
        }

        if (Input.GetAxis("Mouse X") != 0)
        {
            transform.Rotate(new Vector3(0, Input.GetAxis("Mouse X") * sensitivity, 0));
        }
        if (!controller.isGrounded)
        {
            controller.SimpleMove(new Vector3(0, -19.8f, 0));
        }
        animator.SetInteger("speed", (int)(controller.velocity).magnitude);

        if (Input.GetButton("Crouch") && currentStance == stance.standing)
        {
            animator.SetTrigger("Crouch");
            speedMultiplier = 0.5f;
            currentStance = stance.crouched;
        }
        else if(!Input.GetButton("Crouch") && currentStance == stance.crouched)
        {
            animator.SetTrigger("Stand");
            speedMultiplier = 1f;
            currentStance = stance.standing;
        }

    }
}
