﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour {
    public GameObject WalkableSurface;
    public Transform Container;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void SetHidden()
    {
        List<Renderer> Renderers = new List<Renderer>();
        foreach(Renderer item in GetComponentsInChildren<Renderer>() )
        {
            Renderers.Add(item);
        }
        foreach(Renderer item in Container.GetComponentsInChildren<Renderer>() )
        {
            Renderers.Add(item);
        }
        foreach(Renderer item in Renderers)
        {
            item.enabled = false;
            WalkableSurface.GetComponent<Renderer>().enabled = false;
        }
    }
    public void SetVisible()
    {
        List<Renderer> Renderers = new List<Renderer>();
        foreach (Renderer item in GetComponentsInChildren<Renderer>())
        {
            Renderers.Add(item);
        }
        foreach (Renderer item in Container.GetComponentsInChildren<Renderer>())
        {
            Renderers.Add(item);
        }
        foreach (Renderer item in Renderers)
        {
            item.enabled = true;
            WalkableSurface.GetComponent<Renderer>().enabled = true;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        other.transform.SetParent(Container);
        if(other.GetComponent<FloorSwitcher>() != null)
        {
            int i = 0;
            foreach (Floor floor in GameManager.instance.floors)
            {
                if(floor == this)
                {
                    if (i == GameManager.instance.player.floor)
                    {
                        other.GetComponent<FloorSwitcher>().SetVisible();
                    }
                    else
                    {
                        if(other.GetComponent<FloorSwitcher>().common == false)
                            other.GetComponent<FloorSwitcher>().SetHidden();
                    }
                }
                i++;
            }
        }
        if(other.GetComponent<PlayerScript>() != null)
        {
            SetVisible();
            int i = 0;
            bool floored = false;
            foreach (Floor floor in GameManager.instance.floors)
            {
                if (!floored)
                    SetVisible();
                if(floor == this)
                {
                    other.GetComponent<PlayerScript>().floor = i;
                    floored = true;
                }
                if (floor != this && floored)
                {
                    floor.SetHidden();
                }
                i++;
            }
        }

    }
}
