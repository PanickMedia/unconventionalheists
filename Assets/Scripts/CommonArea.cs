﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommonArea : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<FloorSwitcher>() != null)
        {
            other.GetComponent<FloorSwitcher>().common = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<FloorSwitcher>() != null)
        {
            other.GetComponent<FloorSwitcher>().common = false;
        }
    }
}
