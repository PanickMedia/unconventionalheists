﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;

public class SendBehaviorEvent : Interactable {
    public Behavior ai;
    public string messageType;
	// Use this for initialization
	void Start () {
        interactionTimes = null;
        description = " to inspire.";
	}

    public override void DoInteraction(int points)
    {
        base.DoInteraction(points);
        ai.SendEvent(messageType);
    }
    // Update is called once per frame
    void Update () {
		
	}
}
