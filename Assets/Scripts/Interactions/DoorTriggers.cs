﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTriggers : Interactable {
    public bool open = false;
    public bool close = false;
    public bool status = true;
    public bool locked = false;
    Quaternion newRotation;
    Quaternion closedPosition;
	// Use this for initialization
	void Start () {
        closedPosition = transform.rotation;
        if(locked)
        {
            interactionTimes = new int[3];
            interactionTimes[0] = 8;
            interactionTimes[1] = 4;
            interactionTimes[2] = 2;
            description = "pick the lock.";
        }
        else
        {
            interactionTimes = null;
            description = "open the door.";
        }
    }

    



    override public void DoInteraction(int points)
    {
        base.DoInteraction(points);
        if(points != 0)
        {
            if (status == true && open == false && close == false)
            {
                open = true;
                description = "close the door.";
                if(locked)
                {
                    locked = false;
                    interactionTimes = null;
                }
            }
            else if (status == false && open == false && close == false)
            {
                close = true;
                description = "open the door.";
            }
        }else
        {
            GameManager.instance.ai.SendWakingSound(gameObject);
        }
        if(points == 1)
        {
            GameManager.instance.ai.trees.SendEvent("toilet");
        }

    }

    // Update is called once per frame
    void Update () {
		if(open == true && status == true)
        {
            newRotation = closedPosition * Quaternion.Euler(new Vector3(0, 0, -85));
            status = false;
            Collider BlockCollider = GetComponent<Collider>();
            BlockCollider.enabled = false;
        }
        if (close == true && status == false)
        {
            if (ready)
                ready = false;
            newRotation = closedPosition;
            status = true;
        }

        if (open == true && status == false)
        {
            if(ready)
                ready = false;
            if (Mathf.Abs(transform.rotation.eulerAngles.y - newRotation.eulerAngles.y) > 1.1)
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, newRotation, 5f * Time.deltaTime * Time.timeScale);
            }
            else
            {
                transform.rotation = newRotation;
                open = false;
                ready = true;
            }
        }
        if (close == true && status == true)
        {
            if (ready)
                ready = false;
            if (Mathf.Abs(transform.rotation.eulerAngles.y - newRotation.eulerAngles.y) > 1.1)
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, newRotation, 5f * Time.deltaTime * Time.timeScale);
            }
            else
            {
                transform.rotation = closedPosition;
                ready = true;
                close = false;
                Collider BlockCollider = GetComponent<Collider>();
                BlockCollider.enabled = true;
            }
        }
    }
}
