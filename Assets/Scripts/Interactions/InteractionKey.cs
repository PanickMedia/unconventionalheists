﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractionKey : MonoBehaviour {
    private List<Interactable> interactable = new List<Interactable>();
    public Image interactionImg;
    public Text interactionTimeText;
    public Sprite[] interactionStages;
    private int selectedInteraction;
    private int pointVal = 0;
    private int timer = 0;
    private bool interacting;
    // Use this for initialization
	void Start () {
        interactionTimeText = interactionImg.transform.GetChild(0).GetComponent<Text>();
	}
	
    public void SetInteractable(Interactable target)
    {
        interactable.Add(target);
        selectedInteraction = interactable.IndexOf(target);
    }
    public void RemoveInteraction(Interactable target)
    {
        interactable.Remove(target);
        selectedInteraction = 0;
    }
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Interact") && interactable.Count != 0 && interactable[selectedInteraction].ready)
        {
            if(interactable[selectedInteraction].interactionTimes != null)
            {
                interacting = true;
                UpdateSprite();
                interactionImg.gameObject.SetActive(true);
                SetTimer(interactable[selectedInteraction].interactionTimes[0]);
                GameManager.instance.player.movelocked = true;
                GameManager.instance.player.animator.SetTrigger("Interact");
            }
            else
            {
                interactable[selectedInteraction].DoInteraction(3);
                GameManager.instance.player.animator.SetTrigger("qInteract");
            }

        }
        if(interacting && Input.GetButtonUp("Interact") )
        {
            if (interactable[selectedInteraction].interactionTimes != null)
            {
                GameManager.instance.player.movelocked = false;
                interactable[selectedInteraction].DoInteraction(pointVal);
                CancelInvoke();
                interactionImg.gameObject.SetActive(false);
                pointVal = 0;
                interacting = false;
                GameManager.instance.player.animator.SetTrigger("Wake");
            }
        }

	}

    private void SetTimer(int Seconds)
    {
        if (pointVal == 0)
            interactionTimeText.gameObject.SetActive(true);
        else
            interactionTimeText.gameObject.SetActive(false);
        timer = Seconds;
        InvokeRepeating("timeDown", 0.0f, 0.5f);
        interactionTimeText.text = ((Seconds/2) + 1).ToString();
    }

    private void timeDown()
    {
        if (timer > 0)
            timer--;
        else
        {
            CancelInvoke();
            AddPoint();
        }
        interactionTimeText.text = ( (timer/2 )+ 1).ToString();
    }
    private void AddPoint()
    {
        if(pointVal == 0)
        {
            pointVal++;
            SetTimer(interactable[selectedInteraction].interactionTimes[1]);
        }
        else if(pointVal == 1)
        {
            pointVal++;
            SetTimer(interactable[selectedInteraction].interactionTimes[2]);
        }
        else if(pointVal == 2)
        {
            pointVal = 0;
        }
        UpdateSprite();
    }
    private void UpdateSprite()
    {
        interactionImg.sprite = interactionStages[pointVal];
    }
}
