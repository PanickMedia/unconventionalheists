﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour {
    public string description;
    public int[] interactionTimes;
    public bool ready;
    // Use this for initialization
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<InteractionKey>() != null)
        {
            InteractionKey Key = other.GetComponent<InteractionKey>();
            Key.SetInteractable(this);
            if(Key.GetComponent<PlayerScript>() != null && ready)
            {
                if (interactionTimes == null)
                {
                    GameManager.instance.MessageText.text = "Press E to " + description;
                }
                else
                {
                    GameManager.instance.MessageText.text = "Hold E to " + description;
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<InteractionKey>() != null)
        {
            InteractionKey Key = other.GetComponent<InteractionKey>();
            Key.RemoveInteraction(this);
            if (Key.GetComponent<PlayerScript>() != null)
            {
                GameManager.instance.MessageText.text = "";
            }
        }
    }
    virtual public void DoInteraction(int points)
    {
        if (!ready)
            return;
        GameManager.instance.MessageText.text = "";
    }


	// Update is called once per frame
	void Update () {
		
	}
}
