﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    static public GameManager instance;
    public Floor[] floors;
    public Text MessageText;
    public WakingTest ai;
    public PlayerScript player;

    public GameObject HelpPanel;
    public GameObject NotesPanel;
    public GameObject SeenIndicator;
	// Use this for initialization
	void Start () {
        instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButtonDown("Help"))
        {
            HelpPanel.SetActive(!HelpPanel.activeSelf);
        }
        if (Input.GetButtonDown("Notes"))
        {
            NotesPanel.SetActive(!NotesPanel.activeSelf);
        }
    }
}
